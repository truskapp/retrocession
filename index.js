'use strict';

var jade = require('jade');
var wkhtmltopdf = require('wkhtmltopdf');
var path = require('path');
var _ = require('underscore');
var async = require('async');
var moment = require('moment');
var request = require('request');
var sizeOf = require('image-size');
var fs = require('fs');
var config = {};

var splitReg = new RegExp('\ |,', 'ig');

function Retrocession(conf) {
    if (!(this instanceof Retrocession)) {
        return new Retrocession(conf);
    }
    config = conf;
};

var replaceDetails = function(details, data){
    var replacedDetails = [];
    var replaceDetail = function(detail, d){
        _.each(_.keys(d), function(k){
            detail = detail.replace(k.toUpperCase(), d[k]);
        });
        return detail;
    };
    _.each(details, function(dtl){
        if(_.isObject(dtl))
        {
            var replaceObjectDetail = {};
            if(dtl.title)
                replaceObjectDetail.title = replaceDetail(dtl.title, data);
            if(dtl.data)
            {
                var dataDetails = [];
                _.each(dtl.data, function(d){
                    dataDetails.push(replaceDetail(d, data));
                });
                replaceObjectDetail.data = dataDetails;
            }
            replacedDetails.push(replaceObjectDetail);
        }
        else
            replacedDetails.push(replaceDetail(dtl, data));
    });
    return replacedDetails;
}

Retrocession.prototype.generate = function(data, options, callback) {
    var retrocession = _.extend({}, config, options, data);
    retrocession.currency_symbol = retrocession.currency_symbol || '$';
    retrocession.label_retrocession = retrocession.label_retrocession || 'retrocession';
    retrocession.label_retrocession_to = retrocession.label_retrocession_to || 'retrocession to';
    retrocession.label_retrocession_by = retrocession.label_retrocession_by || 'retrocession by';
    retrocession.company_name = retrocession.company_name || 'My company';
    retrocession.date_format = retrocession.date_format || 'DD MMMM YYYY';
    retrocession.client_company_name = retrocession.client_company_name || 'Client Company';
    retrocession.number = retrocession.number || '12345';
    retrocession.currency_position_before = retrocession.currency_position_before || true;
    retrocession.formated_date = moment(retrocession.creation_date).locale(retrocession.language || 'en').format(retrocession.date_format);
    retrocession.pdf_name = retrocession.pdf_name ? (retrocession.pdf_name + '.pdf') : ('INVOICE_' + moment(retrocession.creation_date).format('YYYY-MM-DD') + '_#' + retrocession.number + '.pdf');

    console.log(retrocession)
    if(retrocession.retrocession_details)
        retrocession.retrocession_details = replaceDetails(data.retrocession_details, retrocession);

    var html = jade.renderFile(path.resolve(__dirname + '/templates/retrocession.jade'), {
        retrocession : retrocession,
        cssRessource : [
            path.resolve(path.resolve(__dirname + '/css/retrocession.css')),
            path.resolve(path.resolve(__dirname + '/css/foundation.min.css'))
        ]
    });
    callback(null, retrocession.pdf_name, wkhtmltopdf(html, {pageSize: 'letter'}));
};

module.exports = Retrocession;
