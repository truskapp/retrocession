# Retrocession #

## Render ##

![ScreenShot](/retrocession.jpg)


## Usage ##

### Generate retrocession in a file ####

```
var fs = require('fs');
var retrocession = require('retrocession')({/*options...*/});
var data = {};

retrocession.generate(data, {/*options...*/}, function(error, pdfname, stream){
    stream.pipe(fs.createWriteStream(pdfname));
});
```

### Use with Express ####

```
var express = require('express');
var app = express();
var retrocession = require('retrocession')({/*options...*/});
var data = {};

app.get('/', function(req, res) {
    res.set('content-type', 'application/pdf; charset=utf-8');
    retrocession.generate(data, {/*options...*/}, function(error, pdfname, stream){
        //Force download
        //res.set('Content-Disposition', 'attachment; filename=' + pdfname + '.pdf');
        stream.pipe(res);
    });
});
```

### Options ####

```
number (Number)
currency_position_before (Bool)
currency_symbol (String)
date_format (String)

company_name (String)
company_logo (String)
company_address (String)
company_zipcode (String)
company_city (String)
company_country (String)
company_siret (String)
company_vat_number (String)

client_company_name (String)
client_company_address (String)
client_company_zipcode (String)
client_company_city (String)
client_company_country (String)

label_invoice (String)
label_invoice_to (String)
label_invoice_by (String)
label_invoice_for (String)
label_invoice_by (String)
label_invoice_date (String)
label_invoice_number (String)

```
